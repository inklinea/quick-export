Quick Export - Quickly export Inkscape svg, plain svg, png, pdf and html5 canvas
           
Inkscape 1.1+

* This is not a replacement for saving or Inkscape autosave :) ---

Appears at Extensions>Export

After setting the options in the main dialogue

Assign a shortcut in Inkscape Edit>Preferences>Interface>Keyboard to:

org.inkscape.inklinea.quick_export.noprefs

For shortcut triggered quick export --- Not 100% sure where the PDF and HTML5 settings are pulled from, presume from last used save settings.
